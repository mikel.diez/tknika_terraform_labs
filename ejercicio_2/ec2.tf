
resource "tls_private_key" "key_params" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "tknika_lab2_kp" {
  key_name   = "tknika_lab2_kp"
  public_key = tls_private_key.key_params.public_key_openssh
    provisioner "local-exec" { # Create key on computer
    command = "echo '${tls_private_key.key_params.private_key_pem}' > ./tknika_lab2_kp.pem"
  }
}


data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] 
} # good old 'buntu 20

resource "aws_instance" "web_server" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.public_subnet.id
  vpc_security_group_ids      = [aws_security_group.allow_web.id]
  key_name                    = aws_key_pair.tknika_lab2_kp.key_name
  associate_public_ip_address = true
  user_data                   = <<-EOF
                                #!/bin/bash
                                sudo apt-get update
                                sudo apt-get install -y apache2
                                sudo systemctl start apache2
                                sudo systemctl enable apache2
                                EOF
  tags = {
    Name = "web_server"
  }
}